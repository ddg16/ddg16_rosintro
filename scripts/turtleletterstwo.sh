 #!/usr/bin/bash

rosservice call /turtle1/set_pen 0 255 0 4 true
rosservice call /turtle2/set_pen 255 0 0 8 true
rosservice call /turtle1/teleport_absolute 3 5 0
rosservice call /turtle2/teleport_absolute 6 1 0
rosservice call /turtle1/set_pen 0 255 0 4 false
rosservice call /turtle2/set_pen 255 0 0 8 false

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
 -- '[0.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
 -- '[0.0, -5.0, 0.0]' '[0.0, 0.0, -6.5]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
 -- '[0.0, -4.0, 0.0]' '[0.0, 0.0, -6.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
 -- '[0.0, -1.5, 0.0]' '[0.0, 0.0, 0.0]'

rosparam set /turtlesim/background_r 150
